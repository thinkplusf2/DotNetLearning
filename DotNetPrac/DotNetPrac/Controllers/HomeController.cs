﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DotNetPrac.Models;

namespace DotNetPrac.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            int login = 0;
            return View("index",login);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }

        public ActionResult Login()
        {
            ViewBag.Message = "Login Page";
            return View();
        }

        [HttpPost]
        public ActionResult LoginResult(User Model)
        {

           

            return View("LoginResult", Model);
        }

        public ActionResult Register()
        {
            ViewBag.Message = "Register Page";
            return View();
        }

        [HttpPost]
        public ActionResult RegisterResult(User Model)
        {

            ViewBag.Message = "Register Successed";

            return View("RegisterResult", Model);
        }

        public ActionResult Logout()
        {

            return View("index");
        }

       
    }
}